package com.springboot.point1.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPointOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPointOneApplication.class, args);
	}

}
